#include <stdio.h>
int sumShift(int max) {
   int i, sum = 0;
   for(i = 0; i < max; i++) {
      if(i % 2 == 0) sum += i;
      else sum = sum << 7;
   }
   // printf("Sum = %i\n", sum);
   
   return sum;
}

int sum2() {
   int i, max = 100, sum = 0;
   for(i = 0; i < max; i++) {
      sum += sumShift(i);
   }
   
   return sum;
}

int sum() {
   int i, max = 100, sum = 0;
   for(i = 0; i < max; i++) {
      sum += sum2();
   }
   
   return sum;
}

int main() {
   int ret = sum();
   
   return 0;
}
